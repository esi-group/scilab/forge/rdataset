function datagroups = rdataset_listgroups (  )
// Returns all available datagroups
// 
// Calling Sequence
//   datagroups = rdataset_listgroups (  )
//
// Parameters
//   datagroups : a vector of strings
//
// Examples
//   datagroups = rdataset_listgroups (  )
//
// Authors
//   Copyright (C) 2010 - 2013 - Holger Nahrstaedt
datagroups = ls(rdataset_getpath()+"/csv");



endfunction
