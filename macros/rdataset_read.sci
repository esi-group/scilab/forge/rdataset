function [data,desc] = rdataset_read(datagroup,dataset)
// Reads a dataset from R
// Calling Sequence
// [data,desc] = rdataset_read(dataset)
// [data,desc] = rdataset_read(datagroup,dataset)
// Parameters
// datagroup : string, containing the datagroup in which the dataset is located (default: "datasets")
// dataset :string, containing the dataset, must be located in datagroup
// data : a data structure containing the data from the file
//
// Description
// The data variable has type struct and contains several fields. 
// The fields of the data structure are self-explanatory.
//
// The first field is always a counter, which counts from 1 to N
// 
// All dots (.) in the header are replaced by underline (_). (e.g. survey$Wr.Hnd (from R) is in scilab data.Wr_Hnd)
// 
// quotation marks in all strings are removed ("Never" (from R) is in scilab just Never).
// 
// In desc the short descrition is available for the dataset.
// 
// The type of the field in the struct can either be constant or a string
//
// Examples
// // Read the dataset survey from MASS
// [data,desc] = rdataset_read("MASS","survey")
// // read description
// disp(desc)
// // read header of data
// disp(data(1))
// // Plot age over span of writing hand
// 
// plot(data.Age,data.Wr_Hnd,"bo")
// xlabel("Age");ylabel("span of writing hand");
//
// Bibliography
//  An archive of datasets distributed with R, https://github.com/vincentarelbundock/Rdatasets
//
// Authors
// Copyright (C) 2013 - Holger Nahrstaedt


//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
    
    [lhs,rhs]=argn()
    apifun_checkrhs ( "rdataset_read" , rhs , 1:2 )
    apifun_checklhs ( "rdataset_read" , lhs , 0:2 )
    
    if (rhs==1) then
       dataset=datagroup;
       datagroup="datasets";
    end;
    if (isempty(datagroup)) then
        datagroup="datasets";
    end;
//
//
// Check type
    apifun_checktype ( "rdataset_read" , datagroup , "datagroup" , 1 , "string")
    apifun_checktype ( "rdataset_read" , dataset , "dataset" , 1 , "string")
//
// Read the file
    try 
      filename = rdataset_getpath()+"/csv/"+datagroup+"/"+dataset+".csv";
      if (datagroup+"/"+dataset=="ggplot2/movies" | datagroup+"/"+dataset=="gap/PD"| datagroup+"/"+dataset=="datasets/USJudgeRatings" | datagroup+"/"+dataset=="Ecdat/Males" | datagroup+"/"+dataset=="plm/Males" | datagroup+"/"+dataset=="Zelig/PErisk" | datagroup+"/"+dataset=="vcd/Trucks") then
        csvDefault("separator","%");
      end;
      
      if (datagroup+"/"+dataset=="pscl/AustralianElectionPolling") then
         csvDefault("separator","$");
      end;

      M = read_csv(filename);
      
      
      csvDefault("separator",",");
      

   //M = rdataset_readcsv(filename);
    catch

         error(lasterror());     

    end
    filename_desc = rdataset_getpath()+"/doc/"+datagroup+"/rst/"+dataset+".rst";
     [fd,err]=mopen(filename_desc);
	if ( err <> 0 ) then
	// Temporary workaround for http://bugzilla.scilab.org/show_bug.cgi?id=4833
	//error(msprintf(gettext("%s: Cannot open file %s.\n"),"nistdataset_read",filename))
	error(msprintf(gettext("%s: Cannot open file \n"),"rdataset_read")+filename_desc)
	end
    desc = mgetl(fd);
    err = mclose(fd);

	if ( err <> 0 ) then
	// Temporary workaround for http://bugzilla.scilab.org/show_bug.cgi?id=4833
	//error(msprintf(gettext("%s: Cannot close file %s.\n"),"nistdataset_read",filename))
	error(msprintf(gettext("%s: Cannot close file \n"),"rdataset_read")+filename_desc)
	end
	
	

   

Header = M(1,:);
Header(1)="Nr";


M=M(2:$,:);
elements=size(M,2);
N=size(M,1);

//replace """ by " for all strings in header
//replace . by _
for i=2:elements
 //Header(i)=part(Header(i),2:length(Header(i))-1);
 Header(i)=strsubst(Header(i),".","_");
end


data = tlist(["struct",Header], []);



for i=1:elements

 //test if string or not
 // first column is counter, therefore never string
 if (i==1) then
   M(:,i)=strsubst(M(:,i),"""",""); 
   //data(i+1)=evstr(M(:,i));
   data(i+1)=strtod(M(:,i));
   
 else
 
  isString=%f;
  if isnan(strtod(M(1,i))) & i>1 then
    isString=%t;
  else
    isString=%f;
  end;
  
  if ~isString then
//data(i+1)=evstr(M(:,i));
    //find NAN
    nan_pos=find(M(:,i)=="NA");
    tmp = M(:,i);
    tmp(nan_pos)="%nan";
    tmp = strtod(M(:,i));
    tmp(nan_pos)=%nan;
    data(i+1)=tmp;
    
  else
	//replace """ by "
      M(:,i)=strsubst(M(:,i),"""",""); 

    
      data(i+1)=M(:,i);
    end;
  end;
end; 


endfunction



