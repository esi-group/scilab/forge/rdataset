// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.


//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);

//
// // Generate the ambiguity help
// mprintf("Updating nan/file_io\n");
// helpdir = fullfile(cwd,"file_io");
// funmat = [
//   "xptopen"
//   ];
// macrosdir = cwd +"../../macros/help_files_sci";
// //demosdir = cwd +"../../demos";
// demosdir = [];
// modulename = "nan";
// helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t )

//
// Generate the library help
mprintf("Updating rdataset\n");
helpdir = cwd;
funmat = [
  "rdataset_read"
  "rdataset_getpath"
  "rdataset_listgroups"
  "rdataset_listsets"
  ];
macrosdir = cwd +"../../macros";
demosdir = [];
modulename = "rdataset";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

