mode(1);
//Read the survey dataset from MASS
[data,desc] = rdataset_read("MASS","survey");
//description
halt("press a key");
clc
disp(desc);

//dots in the variable names are replaces by underlines. Wr.Hnd is in scilab Wr_Hnd
halt("press a key");

clc
disp("Mean of the span of writing hand")

mean_hand=mean(thrownan(data.Wr_Hnd))

// In R we can do the same and compare both results
// > library(MASS)
// > x <- survey$Wr.Hnd
// > print(mean(x,na.rm=TRUE),digits=17)                                                                                                                   
//[1] 18.669067796610168

//difference betwenn the mean value which is computed by R and the mean which is computed by scilab:
disp(18.669067796610168-mean_hand)